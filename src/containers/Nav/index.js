import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./estilo.scss";

export class Nav extends Component {

  render() {
    return (
      <div className="container nav">
        <Link to="/">Todos os Registros</Link>
        <Link to="/formulario">Cadastro</Link>
      </div>
    );
  }
}

export default Nav;
