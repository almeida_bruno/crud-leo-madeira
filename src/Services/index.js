export const getRegistros = () => {
  if (!localStorage.getItem("registros")) return [];
  return JSON.parse(localStorage.getItem("registros"));
};

export const getRegistro = (index) => {
  return JSON.parse(localStorage.getItem("registros")).splice(index, 1)[0];
};

export const removeRegistros = (index) => {
  let item = JSON.parse(localStorage.getItem("registros"));
  item.splice(index, 1)
  localStorage.setItem("registros", JSON.stringify(item));
  alert('Registros removido com sucesso!')
  return getRegistros();
};

export const postRegistros = (registro) => {
  let item = JSON.parse(localStorage.getItem("registros")) || []
  item.push(registro);
  localStorage.setItem("registros", JSON.stringify(item));
  alert('Registros criado com sucesso!')

};

export const putRegistros = (registro, index) => {
  let item = JSON.parse(localStorage.getItem("registros"));
  item[index] = registro;
  localStorage.setItem("registros", JSON.stringify(item));
  alert('Registros atualizado com sucesso!')

};
