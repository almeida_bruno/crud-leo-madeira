const validaEmail = email => {
  let re = /\S+@\S+\.\S+/;
  return re.test(email);
}

export default validaEmail
