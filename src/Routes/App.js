import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import Routes from "./index";
import Nav from "../containers/Nav/index";

const App = () => {
  
  return (
    <Router>
      <div className="contentpage container">
        <Nav />
        <Routes />
      </div>
    </Router>
  );
};

export default App;
