import React from "react";
import { Route, Switch } from "react-router-dom";

import Index from "../Pages/Index";
import Formulario from "../Pages/Formulario/index";

const Routes = () => {
  return (
    <main>
      <Switch>
        <Route exact path="/" component={Index} />
        <Route path="/formulario/:id" component={Formulario} />
        <Route path="/formulario" component={Formulario} />
      </Switch>
    </main>
  );
};

export default Routes;
