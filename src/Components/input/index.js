import "./estilo.scss";

export default function Input(props) {
  return (
    <div className={`inputContainer ${props.error ? 'error' : ''}`}>
        <input
          className="input"
          type={props.type}
          placeholder={props.placeholder}
          value={props.value}
          onChange={props.onChange}
          name={props.name}
        />
      {props.error && <span className='error'>{props.error}</span>}
    </div>
  );
}
