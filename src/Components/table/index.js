import Button from "../button";
import "./estilo.scss";

export default function Table(props) {
  return (
    <div className="table-responsive">
      <table>
        <thead>
          <tr>
            {props.column.map((item, index) =>
              <td key={`tr-th-${index}`}>{item.label}</td>
            )}
            <td></td>
            <td></td>
          </tr>
        </thead>

        <tbody>
          {props.data.map((data, index) =>
            <tr key={`tr-${index}`}>

              {props.column.map((column, indexc) =>
                <td key={`tr-td${index}${indexc}`}>
                  {data[column.key]}
                </td>
              )}

              <td><Button type="button" label="Editar" onClick={() => props.onEdit(index)} /></td>
              <td><Button type="button" label="Deletar" onClick={() => props.onDelete(index)} /></td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  );
}
