import InputMask from "react-input-mask";

import "./estilo.scss";

export default function InputMascara(props) {
  return (
    <div className={`inputContainer ${props.error ? 'error': ''}`}>
        <InputMask
          mask={props.mask}
          className='input'
          type={props.type}
          placeholder={props.placeholder}
          value={props.value}
          onChange={props.onChange}
          name={props.name}
        />
      {props.error && <span className='error'>{props.error}</span>}
    </div>
  );
}
