import React, { useState, useEffect } from "react";
import "./estilo.scss";

import Table from "../../Components/table";

import { getRegistros, removeRegistros } from "../../Services"

const COLUMN_DEFAULT = [
  {label: 'Nome', key: 'nome'}, 
  {label:'Cpf', key: 'cpf'}, 
  {label:'Telefone', key: 'telefone'}, 
  {label:'E-mail', key: 'email'}
];

export default function Index(props) {

  const [data, setData] = useState([])
  
  useEffect(() => {
    setData(getRegistros());
  }, []);
  
  const deletar = (index) => {
    setData(removeRegistros(index));
  }


  return (
    <div>
      <h2>Listagem de Registros</h2>
      <Table 
        column={COLUMN_DEFAULT} 
        data={data} 
        onDelete={(index) => deletar(index)}
        onEdit={(index) => props.history.push(`/formulario/${index}`)}
      ></Table>
    </div>
  );
}
