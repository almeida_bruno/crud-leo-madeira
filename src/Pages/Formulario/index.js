import React, { useState } from "react";
import "./estilo.scss";

import Button from "../../Components/button";
import Input from "../../Components/input";
import InputMascara from "../../Components/inputMask";

import { postRegistros, putRegistros, getRegistro } from "../../Services"
import validaCPF from "../../Helpers/validaCPF"
import validaEmail from "../../Helpers/validaEmail"

const DEFAULT_VALUE = {
  nome: "",
  cpf: "",
  email: "",
  telefone: "",
}

export default function Formulario(props) {


  const idItem = props.match.params.id;

  const [fields, setFields] = useState(
    idItem !== undefined
      ? getRegistro(idItem)
      : DEFAULT_VALUE
  );

  const [erros, setErros] = useState({})

  const validaFormulario = () => {
    let newErrors = {};
    let hasError = false;

    if (!validaCPF(fields.cpf)) {newErrors.cpf = "CPF inválido"; hasError = true;}
    if (!validaEmail(fields.email)) {newErrors.email = "E-mail inválido"; hasError = true;}
    if (!fields.nome) {newErrors.nome = "Campo Obrigatório"; hasError = true;}
    if (!fields.email) {newErrors.email = "Campo Obrigatório"; hasError = true;}
    if (!fields.telefone) {newErrors.telefone = "Campo Obrigatório"; hasError = true;}
    if (!fields.cpf) {newErrors.cpf = "Campo Obrigatório"; hasError = true;}

    setErros(newErrors)
    if (!hasError) return true;
    
    return false;
  }

  const salvar = (e) => {
    e.preventDefault();
    if (validaFormulario()) {
      idItem !== undefined
        ? putRegistros(fields, idItem)
        : postRegistros(fields);
    }
  }

  return (
    <div>
      <form onSubmit={(e) => salvar(e)}>
        <Input
          value={fields?.nome}
          onChange={(e) => setFields({ ...fields, nome: e.target.value })}
          name="nome"
          type="text"
          placeholder="Nome completo"
          error={erros?.nome}
        />

        <InputMascara
          value={fields?.cpf}
          onChange={(e) => setFields({ ...fields, cpf: e.target.value })}
          name="cpf"
          type="text"
          placeholder="CPF"
          mask="999.999.999-99"
          error={erros?.cpf}
        />

        <InputMascara
          value={fields?.telefone}
          onChange={(e) => setFields({ ...fields, telefone: e.target.value })}
          name="telefone"
          type="text"
          placeholder="Telefone"
          mask="(99) 9999-99999"
          error={erros?.telefone}
        />

        <Input
          value={fields?.email}
          onChange={(e) => setFields({ ...fields, email: e.target.value })}
          name="email"
          type="email"
          placeholder="E-mail"
          error={erros?.email}
        />

        <Button type="submit" label={idItem === undefined ? 'Salvar' : 'Editar'} />
      </form>
    </div>
  );
}
